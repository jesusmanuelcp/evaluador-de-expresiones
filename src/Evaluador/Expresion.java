/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;

import ufps.util.colecciones_seed.*;

/**
 *
 * @author estudiante
 */
public class Expresion {

    private ListaCD<String> expresiones = new ListaCD();

    public Expresion() {
    }

    public Expresion(String cadena) {

        String v[] = cadena.split(",");
        for (String dato : v) {
            this.expresiones.insertarAlFinal(dato);
        }
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
        String msg = "";
        for (String dato : this.expresiones) {
            msg += dato + "<->";
        }
        return msg;
    }

    
        

    private ListaCD<Character> invertir()
    {    
        if(this.expresiones.esVacia())
        {
        return null;       
        }
   
        ListaCD<Character> invertido=new ListaCD();
        String expresion="";
    for(String datos:this.expresiones)
    {
    expresion+=datos;
    }
    for(int i=expresion.length()-1;i>=0;i--){
        invertido.insertarAlFinal(expresion.charAt(i));
    }
    return invertido;
    }

     public String getPrefijo()
             
    {  
        invertir();
        ListaCD<String> operaciones=new ListaCD();    
        String pre="";
        for(String dato:expresiones)
    {
        if(this.esNumerico(dato)){
            pre+=dato;
        }
        else{
            if(esSigno(dato)){
                operaciones.insertarAlFinal(dato);
            }
            else{
                for(String pilaO:operaciones)
                {
                pre+=pilaO;
                operaciones.eliminar(operaciones.getIndice(pilaO));
                   if(pilaO.equals(")"))
                    break;
                }                 
                }
            }   
    }
        for(String pilaO:operaciones){
        pre+=pilaO;
        }
      
        
       String rta="";
        for(int i=pre.length()-1;i>=0;i--)
        rta+=pre.charAt(i);
        return rta;
    }


/**
 * public String getPrefijo()
 *
 * {
 * invertir(); ListaCD<String> operaciones=new ListaCD(); String pre="";
 * for(String dato:expresiones) { if(this.esNumerico(dato)){ pre+=dato; } else{
 * if(esSigno(dato)){ operaciones.insertarAlFinal(dato); } else{ for(String
 * pilaO:operaciones) { pre+=pilaO;
 * operaciones.eliminar(operaciones.getIndice(pilaO)); if(pilaO.equals(")"))
 * break; } } } } for(String pilaO:operaciones){ pre+=pilaO; }
 *
 * return pre; }
 *
 *
 *
 * /** public String getPrefijo() { ListaCD<Character> expInv =
 * invertirExpresion(); Pila<Character> pila = new Pila(); String prefijoInv =
 * ""; String prefijo = "";
 *
 * for(char dato:expInv) {  *
 * if(dato == '/' || dato == '*' || dato == '-' || dato == '+' || dato == ')') {
 * pila.apilar(dato); } if(dato != '/' && dato != '*' && dato != '-' && dato !=
 * '+' && dato != ')' && dato != '(') { prefijoInv += dato; } if(dato == '(') {
 * char valorTope; do { valorTope = pila.desapilar(); if(valorTope == '/' ||
 * valorTope == '*' || valorTope == '-' || valorTope == '+') prefijoInv +=
 * valorTope; }while(valorTope != ')'); if(!pila.esVacia()) { valorTope =
 * pila.desapilar(); if(valorTope =='*'|| valorTope =='/') { prefijoInv +=
 * valorTope; }else{ pila.apilar(valorTope); } } } } while(!pila.esVacia()) {
 * prefijoInv = prefijoInv + pila.desapilar(); } //inversion de la expresion
 * for(int i = prefijoInv.length()-1;i>=0;i--) { prefijo = prefijo +
 * prefijoInv.charAt(i); } return prefijo;
    }*
 */
public String getPosfijo() {
              
        Pila operadores = new Pila ();
        String expresion = "";
        for (String token : expresiones) {

            if (this.esNumerico(token)) {
                expresion += token + ',';
            } else {
                if (this.esSigno(token)) {
                    if (!operadores.esVacia()) {
                    switch (prioridad(operadores, token)) {
                        case 1:
                            expresion += operadores.getTope().toString() + ',';
                            operadores.apilar(token);
                            
                        case 2:
                            operadores.apilar(token);
                            
                        case 3:
                            while(!operadores.esVacia()){
                                expresion+= operadores.desapilar().toString() + ',';}
                            operadores.apilar(token);
                           
                    }
                     } else {
                        operadores.apilar(token);
                    }
                }
                if (token.equals(")")) {
                    expresion += operadores.toString().replaceAll("->", ",");
                }
            }
        }
        while(!operadores.esVacia()){
                                expresion+= operadores.desapilar() + ",";}

        
        
        return expresion;

    }

    private int prioridad(Pila< String> p, String token) {
        /**
         * si token y tope de pila tienen la misma prioridad =1 si la prioridad
         * de token > tope de pila=3 si la prioridad de token < tope de pila=2
         */

        String h = p.desapilar();
        if (token.equals('*') || token.equals('/')) {

            if (h.equals('*') || h.equals('/')) {
                return 1;
            } else {
                return 3;
            }
        } else {
            if (h.equals('*') || h.equals('/')) {
                return 2;
            } else {
                if (h.equals('+') || h.equals('-')) {
                    return 1;
                } else {
                    return 4;
                }

            }

        }
    }

    public float getEvaluarPosfijo() {
      
        return 0.0f;
    }
   
    

    private boolean esNumerico(String token) {
        try {
            Integer.parseInt(token);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean esSigno(String token) {
        return token.equals('/') || token.equals('*') || token.equals('-') || token.equals('+');
    }

    private boolean esparentesis(String token) {
        return token.equals('[') || token.equals(']') || token.equals("{") || token.equals("}")
                || token.equals('(') || token.equals(")");

    }
}
